name = "usd_al"

version = "0.31.1"

authors = [
    "Deepak Mathur",
    "Brian Silva",
]

description = \
    """
    Animal Logic USD Plugin
    """

external = True

tools = []

requires = ["usd_lib-19.05+"]

uuid = "locksmith_al_usd_plugin"

def commands():
    import os  # This must be imported here. See https://github.com/nerdvegas/rez/wiki/Package-Commands#overview

    al_usd_install = '{}/AL_USD/{}'.format(this.config.release_packages_path, this.version)

    # Check that the usd_install directory is there.
    if not os.path.exists(al_usd_install):
        stop('An AL_USD installation could not be found for version {}. Please contact the pipeline team.'.format(this.version))

    env.MAYA_PLUG_IN_PATH.append(   al_usd_install + '/plugin')
    env.MAYA_SCRIPT_PATH.append(    al_usd_install + '/lib/usd/AL_USDMaya/resources')
    env.XBMLANGPATH.append(         al_usd_install + '/lib/usd/usdMaya/resources')
    env.PYTHONPATH.append(          al_usd_install + '/lib/python')
    env.PATH.append(                al_usd_install + '/lib')

    env.PXR_PLUGINPATH_NAME.append( al_usd_install + '/lib/usd')
    env.AL_USDMAYA_LOCATION = al_usd_install  # For the MayaReference translator

    # These are required for VP2 in Maya versions before 2019.
    # See https://github.com/AnimalLogic/AL_USDMaya/blob/master/docs/build.md#a-note-on-vp2
    env.MAYA_VP2_DEVICE_OVERRIDE = 'VirtualDeviceGL'
    env.MAYA_ENABLE_VP2_PLUGIN_LOCATOR_LEGACY_DRAW = 1
    env.MAYA_VP2_USE_VP1_SELECTION = 1
